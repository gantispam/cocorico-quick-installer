#!/bin/bash

###################################################################################
#
# @script : Install Cocorico with all depencies. 
# @required : A clean VirtualMachine with Centos7
# @author : GSI<gael@sigogneau.fr>
# @since : 09/2019
# @licence : MIT
#
###################################################################################

####### CONSTANTS #################################################################
#     DO NOT CHANGE!
###################################################################################
# logs for installer
PATH_LOG_INSTALLER="/var/log/cocorico-installer.log"
# cocorico folder : path_folder/app_name
PATH_COCO_WWW="/var/www"
# cocorico version
COCO_VERSION="0.15.0"
# cocorico archive name
COCO_ARCHIVE="v${COCO_VERSION}.tar.gz"
# cocorico release
COCO_URI_RELEASE="https://github.com/Cocolabs-SAS/cocorico/archive/${COCO_ARCHIVE}"
# delivery folder
PATH_DELIVERY="/opt/delivery"

####### VARIABLES #################################################################
#     YOU CAN CHANGE THIS VARIABLES FOR PREPARE INSTALLATION
###################################################################################
# global app name : use for web folder's name / database's name / database's user
APP_NAME=""
# unix user for cocorico
UNIX_USER="cocorico"
# unix user's password
UNIX_USER_PWD=""
# mysql pwd for app
APP_DB_PWD=""
# web site DNS
HOST_DNS=""
# unix hostname
HOST_NAME=""
# unix public ip for webserveur
HOST_IP=""
# database root password
NEW_ROOT_PASSWORD=""
# unix timezone
APP_TIMEZONE="Europe/Paris"
# httpd user
APACHE_USER="apache"
# boolean : need ssl or not
HAVE_SSL="no"
# Google Map/Geo API KEY
GOOGLE_API_KEY_BROWSER="null"
GOOGLE_API_KEY_SERVER="null"
GOOGLE_ANALYTICS_KEY="UA-64445856-1"
# Facebook Auth API
FACEBOOK_API_ID="none"
FACEBOOK_API_KEY="none"
# Microsoft translator api key
MICROSOFT_TRANSLATE_KEY="null"

####### INSTALLATION SCRIPT ##########################################################
#
# Check installation init.
#
checkInstallerInit() {
    if (whiptail --title "Setup Cocorico by gael@sigogneau.fr" --yesno "Do you want install Cocorico ${COCO_VERSION} now?\n\nThis installation required :\n* Internet connexion\n* Valide DNS zone as : \"mydomain.com. A x.x.x.x\"" 15 60) then
        # clear logs
        echo "" > ${PATH_LOG_INSTALLER}
        log INFO "Installation is running..."
        # check OS distro
        if [ ! -f /etc/redhat-release ]; then
            log ERROR "This installation need Centos OS. Canceled installation. No change apply on system."
            exit 1;
        fi
        # check root user
        if [ "$(id -u)" != "0" ]; then
            log ERROR "This script must be run as root" 1>&2
            exit 1;
        fi
        #  else : next step
    else
        log WARN "Canceled installation. No change apply on system."
        exit 0;
    fi
}

#
# Implement all variables from user's answers.
#
askCocoricoInstanceInformations() {
    log INFO "[START] Need instance's informations..."

    privateQuestion "Choose an application's name :\nNote :space and special char is forbidden" ${APP_NAME}
    APP_NAME=$(echo ${RESPONSE} | sed -r 's/[-]+/_/g')
    PATH_COCO_WWW="${PATH_COCO_WWW}/${APP_NAME}"
    # check if cocorico is already installed
    if [ -d ${PATH_COCO_WWW} ]; then
        log ERROR "Cocorico ${APP_NAME} is already installed! Uninstall Cocorico ${APP_NAME} before running this installation!"
        exit 1;
    fi

    privateQuestion "Choose your domain adress (DNS) for ${APP_NAME} :\n\nExemple : \"mydomain.com\"" ${HOST_DNS}
    HOST_DNS=${RESPONSE}

    # get all ip and format
    LIST_IP=$(ip addr | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}')
    LIST_IP=echo "$LIST_IP" | sed -r 's/[ ]+/\n/g'
    privateQuestion "Choose public IP (eth) for expose Web server :\n\n${LIST_IP}\n\nExemple: \"88.99.100.22\"" ${HOST_IP}
    HOST_IP=${RESPONSE}

    privateQuestion "Choose your Environnement's HOSTNAME:\nExemple: ${APP_NAME}-integration" ${HOST_NAME}
    HOST_NAME=${RESPONSE}

    privateQuestion "Choose your environnement's timezome :" ${APP_TIMEZONE}
    APP_TIMEZONE=${RESPONSE}

    privateQuestionPass "UNIX - Choose password for UNIX user ${UNIX_USER} :"
    UNIX_USER_PWD=${RESPONSE}

    privateQuestionPass "MYSQL - Choose ROOT password for this MYSQL instance :"
    NEW_ROOT_PASSWORD=${RESPONSE}

    privateQuestionPass "MYSQL - Choose a Database's password for ${APP_NAME}:"
    APP_DB_PWD=${RESPONSE}

    # activate or not SSL
    if (whiptail --title "ENABLE SSL" --yesno "Enable SSL for domain ${HOST_DNS} ?" 10 60) then
        HAVE_SSL="yes"
    fi
    
    log INFO "[END] Need instance's informations"
}

#
# Install all dependencies.
#
installUnixSystem() {
    log INFO "[START] Unix system installation..."

    # change timezone
    timedatectl set-timezone ${APP_TIMEZONE}

    # change unix hostname
    hostname ${HOST_NAME}

    installTools
    installEpel
    installFirewall
    configFirewall
    configUnixUser

    log INFO "[END] Unix system installation"
}

# 
# Update and install default tools on centos
# 
function installTools {
    log INFO "[START] setup tools"
    yum -y update && yum -y install curl telnet wget cronie git zip unzip policycoreutils-python
    
    yum -y install yum-utils
	yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional

    # configure ctontab
    systemctl start crond.service && systemctl enable crond.service
    
    log INFO "[END] setup tools"
}

# 
# Install EPEL for centos 7 64 bits
# 
function installEpel {
    log INFO "[START] setup EPEL"
        wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
        wait
        rpm -ivh epel-release-latest-7.noarch.rpm
        wait
        sleep 10s
        log INFO "Check EPEL repo"
        yum repolist
        yum -y update
        log INFO "[END] setup EPEL"
}

# 
# Install firewalld
# 
function installFirewall {
    log INFO "[START] setup firewalld"
        yum -y install firewalld
        systemctl enable firewalld
        systemctl start firewalld
        wait
    log INFO "Firewwald status :"
        firewall-cmd --state
    log INFO "[END] setup firewalld"
}

# 
# Config firewalld
# 
function configFirewall() {
	log INFO "[START] Security config"
        firewall-cmd --permanent --add-port=80/tcp
        firewall-cmd --permanent --add-port=443/tcp
        firewall-cmd --reload
	log INFO "[END] Security config"
}

# 
# Config unix user and ssh access.
# 
function configUnixUser() {
	log INFO "[START] Config unix user and access"

	log INFO "Create unix user"
        adduser ${UNIX_USER}
        echo "${UNIX_USER_PWD}" | passwd --stdin ${UNIX_USER}

	log INFO "Config unix user"
        echo "shopt -s dotglob" >> /root/.bashrc
        echo "shopt -s dotglob" >> /home/${UNIX_USER}/.bashrc
	    echo "AllowUsers ${UNIX_USER}" >> /etc/ssh/sshd_config
		
	log INFO "Create SSH RSA key for user"
        mkdir -p /home/${UNIX_USER}/.ssh/
        ssh-keygen -f /home/${UNIX_USER}/.ssh/${UNIX_USER}-rsa -t rsa -b 4096 -N ""
        wait
        touch /home/${UNIX_USER}/.ssh/authorized_keys
        cat /home/${UNIX_USER}/.ssh/${UNIX_USER}-rsa.pub > /home/${UNIX_USER}/.ssh/authorized_keys
        chown -R ${UNIX_USER}:${UNIX_USER} /home/${UNIX_USER}/.ssh/
        wait

	log INFO "[END] Config unix user and access"
}

# 
# Install CERTBOT.
# 
installCertBot() {
    if [ "x${HAVE_SSL}" = "xyes" ]; then
        log INFO "[START] CertBot installation..."   
            # install certbot
            yum install -y certbot python2-certbot-apache
            # create cronjob
            echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot renew" | sudo tee -a /etc/crontab > /dev/null
            sleep 5s
        log INFO "[END] CertBot installation"
    else
        log INFO "CERTBOT was not installed"
    fi
}

# 
# Install HTTPD and PHP.
# 
installHttpd() {
    log INFO "[START] HTTPD installation..."    

    # create SSL certificat
    certbot certonly --standalone --domain ${HOST_DNS} --register-unsafely-without-email

    # install php 7.2 (7.3 isn't compatible)
    rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm && yum --enablerepo=remi-php72 install -y php
    
    # install httpd + php + modules
    yum --enablerepo=remi-php72 install -y php php-xml php-xmlrpc php-mbstring php-json php-gd php-mcrypt php-intl php-soap php-mongodb php-imagick php-mysql php-opcache php-apcu php-pear php-apc php-zip mod_ssl
    
    # config PHP
    # find /etc/php.ini -type f -exec sed -i 's/;curl.cainfo =/curl.cainfo = "pathto/cacert.pem"/g' {} \;
    find /etc/php.ini -type f -exec sed -i 's/memory_limit = 128M/memory_limit = 256M/g' {} \;
    find /etc/php.ini -type f -exec sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 12M/g' {} \;
    find /etc/php.ini -type f -exec sed -i 's/post_max_size = 8M/post_max_size = 240M/g' {} \;
    find /etc/php.ini -type f -exec sed -i 's/;date.timezone =/date.timezone = UTC/g' {} \;

	# hide server informations
	echo "ServerTokens Prod" >> /etc/httpd/conf/httpd.conf
	echo "ServerSignature Off " >> /etc/httpd/conf/httpd.conf
	
	# remove welcome page
	echo "" > /etc/httpd/conf.d/welcome.conf
	
	# cocorico virtualhost
	echo "" > /etc/httpd/conf.d/99-cocorico.conf

    if [ "x${HAVE_SSL}" = "xyes" ]; then

    # create certificats
    certbot certonly --standalone --domain ${HOST_DNS}
    sleep 5s

    # create httpd SSL configuration
cat <<- EOF > /etc/httpd/conf.d/99-cocorico.conf
#
# COCORICO : VIRTUALHOST CONFIGURATION
#

ServerName ${HOST_NAME}

<VirtualHost ${HOST_IP}:80>
    ServerName ${HOST_DNS}
    ServerAlias www.${HOST_DNS}
    Redirect permanent / https://${HOST_DNS}/
</VirtualHost>

<VirtualHost ${HOST_IP}:443>
    ServerName ${HOST_DNS}
    ServerAlias www.${HOST_DNS}
    DocumentRoot "${PATH_COCO_WWW}/web"

	ErrorLog "|/usr/sbin/rotatelogs /var/log/httpd/${APP_NAME}/error.%Y-%m-%d.log 86400"
	CustomLog "|/usr/sbin/rotatelogs -l /var/log/httpd/${APP_NAME}/access.%Y-%m-%d.log 86400" combined 
	
    SSLEngine on
    SSLCertificateFile "/etc/letsencrypt/live/${HOST_DNS}/cert.pem"
    SSLCertificateKeyFile "/etc/letsencrypt/live/${HOST_DNS}/privkey.pem"

    <Directory ${PATH_COCO_WWW}/web>
        DirectoryIndex app.php
        AllowOverride None

        LimitRequestBody 240000000

        <Files ~ "^\.ht">
            Order deny,allow
            Deny from all
        </Files>

        <IfModule mod_rewrite.c>
            RewriteEngine On

            RewriteCond %{REQUEST_URI}::$1 ^(/.+)/(.*)::\2$
            RewriteRule ^(.*) - [E=BASE:%1]

            RewriteCond %{HTTP:Authorization} .
            RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

            RewriteCond %{ENV:REDIRECT_STATUS} ^$
            RewriteRule ^app\.php(/(.*)|$) %{ENV:BASE}/$2 [R=301,L]

            RewriteCond %{REQUEST_FILENAME} -f
            RewriteRule .? - [L]

            RewriteRule .? %{ENV:BASE}/app.php [L]
        </IfModule>

        <IfModule !mod_rewrite.c>
            <IfModule mod_alias.c>
                RedirectMatch 302 ^/$ /app.php/
            </IfModule>
        </IfModule>

        <IfModule mod_expires.c>
            ExpiresActive on
            ExpiresByType image/jpg "access plus 60 days"
            ExpiresByType image/png "access plus 60 days"
            ExpiresByType image/gif "access plus 60 days"
            ExpiresByType image/jpeg "access plus 60 days"
            ExpiresByType text/css "access plus 1 days"
            ExpiresByType image/x-icon "access plus 1 month"
            ExpiresByType application/pdf "access plus 1 month"
            ExpiresByType audio/x-wav "access plus 1 month"
            ExpiresByType audio/mpeg "access plus 1 month"
            ExpiresByType video/mpeg "access plus 1 month"
            ExpiresByType video/mp4 "access plus 1 month"
            ExpiresByType video/quicktime "access plus 1 month"
            ExpiresByType video/x-ms-wmv "access plus 1 month"
            ExpiresByType application/x-shockwave-flash "access 1 month"
            ExpiresByType text/javascript "access plus 1 week"
            ExpiresByType application/x-javascript "access plus 1 week"
            ExpiresByType application/javascript "access plus 1 week"
            ExpiresByType application/vnd.bw-fontobject "access plus 30 days"
            ExpiresByType application/x-font-ttf "access plus 30 days"
            ExpiresByType application/x-woff "access plus 30 days"  
        </IfModule>

        AddOutputFilterByType DEFLATE text/html text/css application/x-javascript application/x-shockwave-flash
        # Cope with proxies
        Header append Vary User-Agent env=!dont-vary
        # Cope with several bugs in IE6
        BrowserMatch "\bMSIE 6" !no-gzip !gzip-only-text/html
    </Directory>

    <Directory ${PATH_COCO_WWW}/web/uploads>
        Deny from all

        <Files ^(*.jpeg|*.jpg|*.png|*.gif|*.pdf)>
            Order deny,allow
            Allow from all
        </Files>

        <Files ~ "^\.ht">
            Order deny,allow
            Deny from all
        </Files>
    </Directory>

    <IfModule mod_fcgid.c>
        IPCCommTimeout          180
        IPCConnectTimeout       180
    </IfModule>
</VirtualHost>
EOF
	
    else
    # without SSL configuration
cat <<- EOF > /etc/httpd/conf.d/99-cocorico.conf
#
# COCORICO : VIRTUALHOST CONFIGURATION
#

ServerName ${HOST_NAME}

<VirtualHost ${HOST_IP}:80>
    ServerName ${HOST_DNS}
    ServerAlias www.${HOST_DNS}
    DocumentRoot "${PATH_COCO_WWW}/web"

	ErrorLog "|/usr/sbin/rotatelogs /var/log/httpd/${APP_NAME}/error.%Y-%m-%d.log 86400"
	CustomLog "|/usr/sbin/rotatelogs -l /var/log/httpd/${APP_NAME}/access.%Y-%m-%d.log 86400" combined 
	
    <Directory ${PATH_COCO_WWW}/web>
        DirectoryIndex app.php
        AllowOverride None

        LimitRequestBody 240000000

        <Files ~ "^\.ht">
            Order deny,allow
            Deny from all
        </Files>

        <IfModule mod_rewrite.c>
            RewriteEngine On

            RewriteCond %{REQUEST_URI}::$1 ^(/.+)/(.*)::\2$
            RewriteRule ^(.*) - [E=BASE:%1]

            RewriteCond %{HTTP:Authorization} .
            RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

            RewriteCond %{ENV:REDIRECT_STATUS} ^$
            RewriteRule ^app\.php(/(.*)|$) %{ENV:BASE}/$2 [R=301,L]

            RewriteCond %{REQUEST_FILENAME} -f
            RewriteRule .? - [L]

            RewriteRule .? %{ENV:BASE}/app.php [L]
        </IfModule>

        <IfModule !mod_rewrite.c>
            <IfModule mod_alias.c>
                RedirectMatch 302 ^/$ /app.php/
            </IfModule>
        </IfModule>

        <IfModule mod_expires.c>
            ExpiresActive on
            ExpiresByType image/jpg "access plus 60 days"
            ExpiresByType image/png "access plus 60 days"
            ExpiresByType image/gif "access plus 60 days"
            ExpiresByType image/jpeg "access plus 60 days"
            ExpiresByType text/css "access plus 1 days"
            ExpiresByType image/x-icon "access plus 1 month"
            ExpiresByType application/pdf "access plus 1 month"
            ExpiresByType audio/x-wav "access plus 1 month"
            ExpiresByType audio/mpeg "access plus 1 month"
            ExpiresByType video/mpeg "access plus 1 month"
            ExpiresByType video/mp4 "access plus 1 month"
            ExpiresByType video/quicktime "access plus 1 month"
            ExpiresByType video/x-ms-wmv "access plus 1 month"
            ExpiresByType application/x-shockwave-flash "access 1 month"
            ExpiresByType text/javascript "access plus 1 week"
            ExpiresByType application/x-javascript "access plus 1 week"
            ExpiresByType application/javascript "access plus 1 week"
            ExpiresByType application/vnd.bw-fontobject "access plus 30 days"
            ExpiresByType application/x-font-ttf "access plus 30 days"
            ExpiresByType application/x-woff "access plus 30 days"  
        </IfModule>

        AddOutputFilterByType DEFLATE text/html text/css application/x-javascript application/x-shockwave-flash
        # Cope with proxies
        Header append Vary User-Agent env=!dont-vary
        # Cope with several bugs in IE6
        BrowserMatch "\bMSIE 6" !no-gzip !gzip-only-text/html
    </Directory>

    <Directory ${PATH_COCO_WWW}/web/uploads>
        Deny from all

        <Files ^(*.jpeg|*.jpg|*.png|*.gif|*.pdf)>
            Order deny,allow
            Allow from all
        </Files>

        <Files ~ "^\.ht">
            Order deny,allow
            Deny from all
        </Files>
    </Directory>

    <IfModule mod_fcgid.c>
        IPCCommTimeout          180
        IPCConnectTimeout       180
    </IfModule>
</VirtualHost>
EOF
    fi

    # allow on SELinux
	setsebool -P httpd_can_network_connect on
	
    # add app user on apache groupe
    HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
    usermod -aG ${APACHE_USER} ${UNIX_USER}
    usermod -aG ${UNIX_USER} ${APACHE_USER}
    
    # prepare logs folder
    mkdir -p /var/log/httpd/${APP_NAME} && chown -R ${APACHE_USER}:${APACHE_USER} /var/log/httpd/${APP_NAME}

    # restart httpd
    systemctl start httpd && systemctl enable httpd && systemctl status httpd
    sleep 5s
    log INFO "[END] HTTPD installation"   
}

#
# Install MongoDB.
#
installMongoDB() {
    if (whiptail --title "MongoDB Installer" --yesno "Do you want install MongoDB on this machine?\nNote : if you answer NO you need to install and configure MongoDB yourself)" 10 60) then
        log INFO "[START] MongoDB installation"
        echo "" > /etc/yum.repos.d/mongodb-org-4.2.repo
cat <<- EOF > /etc/yum.repos.d/mongodb-org-4.2.repo
[mongodb-org-4.2]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/\$releasever/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
EOF
        yum install -y mongodb-org
        wait
        systemctl start mongod && systemctl enable mongod && systemctl status mongod
        sleep 5s

        log INFO "[END] MongoDB installation"
    else
        log WARN "MongoDB is not installed. You need install and configure MongoDB yourself"
    fi

}

#
# Install Mysql.
#
installMysql() {
    if (whiptail --title "Mysql Installer" --yesno "Do you want install Mysql on this machine?\nNote : if you answer NO you need to install and configure Mysql yourself)" 10 60) then
        # install mysql
        log INFO "[START] Mysql installation"
            RPM_MYSQL="mysql57-community-release-el7-7.noarch.rpm"
            wget "http://dev.mysql.com/get/${RPM_MYSQL}"
            wait
            if [ "$?" -ne 0 ]; then
                log ERROR "Fail to download Mysql : ${RPM_MYSQL}"
                exit 1;
            fi
            yum -y localinstall ${RPM_MYSQL}
            wait
            yum-config-manager --enable mysql57-community
            wait
            yum -y install mysql-community-server
            wait
            systemctl start mysqld.service && systemctl enable mysqld.service && systemctl status mysqld
            sleep 5s
            rm -f ${RPM_MYSQL}
        log INFO "[END] Mysql installation"

        # configure mysql
        log INFO "[START] Mysql configuration"

            TEMP_ROOT_PASSWORD=$(grep 'temporary password' /var/log/mysqld.log | awk '{ print $NF }')
            CMD_FILE="./mysql-temp-cmd.sql"
            echo "" > ${CMD_FILE}

            # disable password policy
            echo "ALTER USER 'root'@'localhost' IDENTIFIED BY 'a7X6q?Zu/1';" > ${CMD_FILE}
            echo "uninstall plugin validate_password;" >> ${CMD_FILE}
            mysql --user=root --password=$TEMP_ROOT_PASSWORD --connect-expired-password < ${CMD_FILE}

            # change root password
            echo "ALTER USER 'root'@'localhost' IDENTIFIED BY '${NEW_ROOT_PASSWORD}';" > ${CMD_FILE}
            mysql --user=root --password=a7X6q?Zu/1 --connect-expired-password < ${CMD_FILE}
            
            # create database and user
            echo "CREATE DATABASE IF NOT EXISTS ${APP_NAME} DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;" > ${CMD_FILE}
            echo "CREATE USER IF NOT EXISTS '${APP_NAME}'@'localhost' IDENTIFIED BY '${APP_DB_PWD}';" >> ${CMD_FILE}
            echo "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, REFERENCES ON ${APP_NAME}.* TO ${APP_NAME}@localhost IDENTIFIED BY '${APP_DB_PWD}';" >> ${CMD_FILE}
            echo "FLUSH PRIVILEGES;" >> ${CMD_FILE}
            mysql --user=root --password=${NEW_ROOT_PASSWORD} --connect-expired-password < ${CMD_FILE}

            # fixe group_by sql instruction
            echo "sql_mode=\"STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\"" >> /etc/my.cnf
            systemctl restart mysqld && systemctl status mysqld
            sleep 5s
    
            rm -f ${CMD_FILE} 2>/dev/null
        log INFO "[END] Mysql configuration"
    else
        log WARN "Mysql is not installed. You need install and configure Mysql yourself"
    fi
}

#
# Install Cocorico instance.
#
installCocorico() {
    log INFO "[START] Cocorico installation..."
        V_NOW_DAY=`date '+%Y%m%d'`

    log INFO "Composer installation..."
        curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    
    log INFO "Download cocorico..."
        mkdir -p ${PATH_COCO_WWW} && cd ${PATH_COCO_WWW} 
        wget ${COCO_URI_RELEASE} && tar -xvf ${COCO_ARCHIVE} --strip 1
    
    log INFO "Prepare cocorico..."
        mkdir -p ${PATH_COCO_WWW}/var
        mkdir -p ${PATH_COCO_WWW}/web/media/cache/user_small/uploads
        mkdir -p ${PATH_DELIVERY}/${V_NOW_DAY}_fresh-install && mv ${COCO_ARCHIVE} ${PATH_DELIVERY}/${V_NOW_DAY}_fresh-install/
        rm -rf ${PATH_COCO_WWW}/.github
        rm -rf ${PATH_COCO_WWW}/*.md

    # config cocorico instance
    if [[ ! -f app/config/parameters.yml ]]; then
        FILE_PARAMETERS=${PATH_COCO_WWW}/app/config/parameters.yml

        # SMTP info
        if (whiptail --title "MAIL SERVER - SMTP" --yesno "Do you want configure SMTP now?" 10 60) then
            log INFO "Configure SMTP informations..."

                privateQuestion "MAIL - SMTP HOST :" "127.0.0.1"
                SMTP_HOST=${RESPONSE}
                find ${FILE_PARAMETERS} -type f -exec sed -i 's/mailer_host:       127.0.0.1/mailer_host:       '"${SMTP_HOST}"'/g' {} \;

                privateQuestion "MAIL - SMTP PORT :" "25"
                SMTP_PORT=${RESPONSE}
                find ${FILE_PARAMETERS} -type f -exec sed -i 's/mailer_port: null/mailer_port: '"${SMTP_PORT}"'/g' {} \;

                privateQuestion "MAIL - SMTP USER :" "~"
                SMTP_USER=${RESPONSE}
                find ${FILE_PARAMETERS} -type f -exec sed -i 's/mailer_user:       ~/mailer_user:       '"${SMTP_USER}"'/g' {} \;
                
                privateQuestion "MAIL - SMTP PASSWORD :" "~"
                SMTP_PASS=${RESPONSE}
                find ${FILE_PARAMETERS} -type f -exec sed -i 's/mailer_password:   ~/mailer_password:   '"${SMTP_PASS}"'/g' {} \;
        fi
        
        # GOOGLE MAP/GEO API INFO
        if (whiptail --title "GOOGLE API - KEYS" --yesno "Do you want configure Google Map/Geo API now?\n\nYou may need to read documentations : https://tinyurl.com/y3tdleg5\nhttps://tinyurl.com/yxtdapdb" 13 60) then
            log INFO "Configure Google Map/Geo API..."

                privateQuestion "Enter BROWSER GoogleAPI KEY (without restriction):" "${GOOGLE_API_KEY_BROWSER}"
                GOOGLE_API_KEY_BROWSER=${RESPONSE}

                privateQuestion "Enter SERVER GoogleAPI KEY (with restriction):" "${GOOGLE_API_KEY_SERVER}"
                GOOGLE_API_KEY_SERVER=${RESPONSE}
        fi

        # FACEBOOK AUTH API INFO
        if (whiptail --title "FACEBOOK API - KEYS" --yesno "Do you want configure Facebook Auth API now?\n\nYou may need to read documentations : \nhttps://tinyurl.com/yxtdapdb" 13 60) then
            log INFO "Configure Facebook Auth API..."

                privateQuestion "Enter Facebook API ID :" "${FACEBOOK_API_ID}"
                FACEBOOK_API_ID=${RESPONSE}

                privateQuestion "Enter Facebook API KEY :" "${FACEBOOK_API_KEY}"
                FACEBOOK_API_KEY=${RESPONSE}
        fi

        # MICROSOFT TRANSLATOR API INFO
        if (whiptail --title "MICROSOFT API - KEYS" --yesno "Do you want configure Microsoft translator  API now?\n\nYou may need to read documentations : \nhttps://tinyurl.com/yxtdapdb" 13 60) then
            log INFO "Configure Microsoft translator API..."
                privateQuestion "Enter Microsoft API KEY :" "${MICROSOFT_TRANSLATE_KEY}"
                MICROSOFT_TRANSLATE_KEY=${RESPONSE}
        fi

        # GOOGLE ANALYTICS API INFO
        if (whiptail --title "GOOGLE API - KEYS" --yesno "Do you want configure Google Analytics KEY now?" 13 60) then
            log INFO "Configure Google analytics API..."
                privateQuestion "Enter Google Analytics KEY (you can keep default value = disable):" "${GOOGLE_ANALYTICS_KEY}"
                GOOGLE_ANALYTICS_KEY=${RESPONSE}
        fi

        # build secret key
        log INFO "Generate secret key..."
            RMND_SECRET_KEY=$(curl http://nux.net/secret | grep "class=\"lead\"" | cut -c17-56)

        # build app URL
        URL_APP="http://${HOST_DNS}"
        if [ "x${HAVE_SSL}" = "xyes" ]; then
            URL_APP="https://${HOST_DNS}"
        fi

echo "" > ${FILE_PARAMETERS}
cat <<- EOF > ${FILE_PARAMETERS}
parameters:
    database_host: 127.0.0.1
    database_port: 3306
    database_name: ${APP_NAME}
    database_user: ${APP_NAME}
    database_password: ${APP_DB_PWD}
    mongodb_server: 'mongodb://localhost:27017'
    mongodb_database_name: ${APP_NAME}
    mailer_transport: smtp
    mailer_host: ${SMTP_HOST}
    mailer_user: ${SMTP_USER}
    mailer_password: ${SMTP_PASS}
    mailer_port: ${SMTP_PORT}
    secret: ${RMND_SECRET_KEY}
    use_assetic_controller: false
    cocorico.assets_base_urls: '${URL_APP}'
    router.request_context.host: ${HOST_DNS}
    router.request_context.scheme: http
    router.request_context.base_url: ''
    cocorico.admin: admin
    cocorico.admin_translation: false
    cocorico.check_translation: false
    cocorico.translator.secret.key: ${MICROSOFT_TRANSLATE_KEY}
    cocorico.facebook.app_id: ${FACEBOOK_API_ID}
    cocorico.facebook.secret: ${FACEBOOK_API_KEY}
    cocorico.image_driver: imagick
    cocorico.google_analytics: ${GOOGLE_ANALYTICS_KEY}
    cocorico.google_tag_manager: false
    cocorico_geo.google_place_api_key: ${GOOGLE_API_KEY_BROWSER}
    cocorico_geo.google_place_server_api_key: ${GOOGLE_API_KEY_SERVER}
    cocorico_geo.ipinfodb_api_key: ${GOOGLE_API_KEY_BROWSER}
    cocorico.deploy.host: ${HOST_DNS}
    cocorico.deploy.dir: ${PATH_COCO_WWW}
    cocorico.deploy.user: ${APACHE_USER}
    cocorico.booking.expiration_delay: 2880
    cocorico.booking.acceptation_delay: 240
    cocorico.booking.alert_expiration_delay: 120
    cocorico.booking.alert_imminent_delay: 1440
    cocorico.booking.validated_moment: start
    cocorico.booking.validated_delay: 0
    cocorico.bankwire_checking_simulation: false
EOF

    # create a config backup
    cp ${FILE_PARAMETERS} ${PATH_COCO_WWW}/app/config/parameters.yml.bak
    
    fi

    if [[ ! -d vendor ]]; then
        log INFO "[START] Download Cocorico dependencies..."
        composer install --prefer-dist
        log INFO "[END] Download Cocorico dependencies"
    fi

    while !(mysqladmin -u${APP_NAME} -p${APP_DB_PWD} ping &> /dev/null); do
        log INFO "[WAIT] Waiting Database connexion..."
        sleep 1s
    done

    log INFO "[INFO] Create SQL Database tables..."
        php ${PATH_COCO_WWW}/bin/console doctrine:schema:update --force
        php ${PATH_COCO_WWW}/bin/console doctrine:fixtures:load -n

    log INFO "[INFO] Create NOSQL Database schema..."
        php ${PATH_COCO_WWW}/bin/console doctrine:mongodb:schema:create
   
    log INFO "[INFO] Install CKEditor...."
        php ${PATH_COCO_WWW}/bin/console ckeditor:install

    log INFO "[INFO] Update Cocorico currencies..."
        php ${PATH_COCO_WWW}/bin/console cocorico:currency:update
   
    log INFO "[INFO] Install Cocorico assets..."
        php ${PATH_COCO_WWW}/bin/console assets:install --symlink
    
    log INFO "[INFO] Compile assets resources..."
        php ${PATH_COCO_WWW}/bin/console assetic:dump
   
    log INFO "[INFO] Warmup prod. config...."
        php ${PATH_COCO_WWW}/bin/console cache:warmup --env=prod
        php ${PATH_COCO_WWW}/bin/console cache:clear --env=prod

    # file/user permissions
    log INFO "[INFO] Set Unix permissions..."
        # access permissions
        chmod -R 775 ${PATH_COCO_WWW}
        # permissions for app user
        chown -R ${UNIX_USER}:${UNIX_USER} ${PATH_COCO_WWW}
        chown -R ${UNIX_USER}:${UNIX_USER} ${PATH_DELIVERY}
        
        # permissions for apache user
        setfacl -dR -m u:"$APACHE_USER":rwX ${PATH_COCO_WWW}/var
        setfacl -R -m u:"$APACHE_USER":rwX ${PATH_COCO_WWW}/var
        setfacl -dR -m u:"$APACHE_USER":rwX ${PATH_COCO_WWW}/web/media
        setfacl -R -m u:"$APACHE_USER":rwX ${PATH_COCO_WWW}/web/media

        # SELINUX permissions
        restorecon -R ${PATH_COCO_WWW}
        wait
        semanage fcontext -a -t httpd_sys_rw_content_t "${PATH_COCO_WWW}(/.*)?"
        wait
        semanage fcontext -a -t httpd_sys_rw_content_t "${PATH_COCO_WWW}/var(/.*)?"
        wait
        semanage fcontext -a -t httpd_sys_rw_content_t "${PATH_COCO_WWW}/web/uploads(/.*)?"
        wait
        semanage fcontext -a -t httpd_sys_rw_content_t "${PATH_COCO_WWW}/web/media(/.*)?"
        wait
        restorecon -R ${PATH_COCO_WWW}

    log INFO "[END] Restart httpd"
        systemctl restart httpd
        sleep 5s

    log INFO "[END] Cocorico installation"
}

#
# End of installation.
#
installationEnd() {
    log INFO "Installation End"
    MSG_END="Installation end.\nMaybe you need to check logs and your installation now :\n\n* URL: http://${HOST_DNS}\n* LOGIN: super-admin@cocorico.rocks\n* PASS: super-admin"
    whiptail --title "INSTALLATION END" --msgbox "${MSG_END}" 14 60
}

#
# Ask question.
# 
# @param (mandatory) QUESTION user's question
# @param (optional) DEFAULT_VALUE default value
# @param (optional) BLOCK_INSTALL=true do not stop installation script on cancelled event (by default=true)
# @return $RESPONSE user's response
#
function privateQuestion() {
    QUESTION=$1
    DEFAULT_VALUE=$2
    BLOCK_INSTALL=$3
    RESPONSE=""

    WARN_MESSAGE=""
    if [ "x${BLOCK_INSTALL}" = "x" ] || [ "x${BLOCK_INSTALL}" = "xtrue" ]; then
        WARN_MESSAGE="\n\nWARN : Cancel option will stop this installation..."
    fi

    # GET DOMAIN
    RESPONSE=$(whiptail --title "CONFIGURATION" --inputbox "$QUESTION $WARN_MESSAGE" 14 60 ${DEFAULT_VALUE} 3>&1 1>&2 2>&3)
    exitstatus=$?
    # check response
    if [ $exitstatus = 0 ]; then
        # check response is not empty
        if [ "x${RESPONSE}" = "x" ]; then
            privateQuestion "${QUESTION}"
        fi
        log INFO "QUESTION : ${QUESTION} = ${RESPONSE}"
    else
        log INFO "Canceled Installation by user !"
        if [ "x${BLOCK_INSTALL}" = "x" ] || [ "x${BLOCK_INSTALL}" = "xtrue" ]; then
            exit 1;
        fi
    fi
}

#
# Ask question for password data.
# 
# @param (mandatory) user's question
# @return $RESPONSE user's response
#
function privateQuestionPass() {
    RESPONSE=$(whiptail --title "PASSWORD" --passwordbox "$1" 10 60 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        if [ "x${RESPONSE}" = "x" ]; then
            privateQuestionPass "$1"
        else
            log INFO "Password was set"
        fi
    else
        log WARN "Password was not set!"
    fi
}

#
# Logger
#
# @param log_level
# @param log_message
#
function log() {
    LOG_DATE_NOW=`date '+%Y-%m-%d-%H-%M-%S'`
    # cyan
    TAG_TYPE="\033[46m[$1]\033[0m"
    if [ "x$1" = 'xERROR' ]; then
        # red
        TAG_TYPE="\033[41m[$1]\033[0m"
        >&2 echo -e "\n$TAG_TYPE - $LOG_DATE_NOW - $2"
    fi
    if [ "x$1" = 'xWARN' ]; then
        # yellow
        TAG_TYPE="\033[43m[$1]\033[0m"
    fi
    if [ "x$1" = 'xINFO' ]; then
        # blue
        TAG_TYPE="\033[44m[$1]\033[0m"
    fi
    if [ "x$1" = 'xSUCCESS' ]; then
        # green
        TAG_TYPE="\033[42m[$1]\033[0m"
    fi

    if [ "x$1" != 'xERROR' ]; then
        echo -e "\n$TAG_TYPE - $LOG_DATE_NOW - $2"
    fi
    echo -e "\n$TAG_TYPE - $LOG_DATE_NOW - $2" >> ${PATH_LOG_INSTALLER}
}


    ####### MAIN ENTRYPOINT ############################################################
    #
    # INSTALLATION WORKFLOW
    #
    ###################################################################################
    function entryPoint() {
        checkInstallerInit
        askCocoricoInstanceInformations
        installUnixSystem
        installCertBot
        installHttpd
        installMongoDB
        installMysql
        installCocorico
        installationEnd
    }
    entryPoint