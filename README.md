<h1 align="center">
    <img src="http://docs.cocorico.io/images/logo_cocorico.png" alt="Cocorico"/>
</h1>

# About Cocorico framework
## Cocorico informations
[![Try the demo](https://img.shields.io/badge/try-demo-green.svg)](http://demo.cocorico.io)
[![Build Status](https://secure.travis-ci.org/Cocolabs-SAS/cocorico.svg)](http://travis-ci.org/Cocolabs-SAS/cocorico)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Cocorico is an open source platform sponsored by [Cocolabs](http://www.cocolabs.io) to create collaborative consumption marketplaces.

## Informations
* Official website : https://www.cocolabs.io/en/
* Official sources : https://github.com/Cocolabs-SAS/cocorico
* Official documentation : https://github.com/Cocolabs-SAS/cocorico/tree/master/doc


# About Cocorico installer

##
* This script is unofficial
* This script is offer without guarantee

## Required
* VirtualMachine with Centos7
* Root access
* Internet
* Valid DNS -> VirtualMachine
* Some additional information can be helpful : 
  * Smtp informations
  * Google Api key (Map,Geo,Analytics)
  * Facebook Api Key
  * Microsoft Translator Api key
  * ...

# How install Cocorico with cocorico-installer.sh ?
* Connect on your server with user : ```root```
* Add `cocorico-installer.sh` on your server
* Running installation with logs trace : 
```console
chmod +x ./cocorico-installer.sh && ./cocorico-installer.sh
```

* This script install and configure : 
    * unix user "cocorico" with RSA key for CC deploymnt (mandatory) 
    * Httpd 2.4 (mandatory) 
    * Php 7.2 (mandatory) 
    * MongoDB 4.2 (mandatory) 
    * Mysql 5.7 (mandatory) 
    * Certbot (optional) 
    * Cocorico 0.15.0 (mandatory)
* You can found installation logs on : 
    * ```/var/log/cocorico-installer.log```